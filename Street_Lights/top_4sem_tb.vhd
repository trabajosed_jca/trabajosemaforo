----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2018 19:33:33
-- Design Name: 
-- Module Name: top_4sem_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top_4sem_tb is
--  Port ( );
end top_4sem_tb;

architecture Behavioral of top_4sem_tb is
    component div_frec
    port(
        clk: in std_logic;
        new_clk: out std_logic
    );
    end component;
    component sem1 
    port(
        clk: in std_logic;
        clk_cont: in std_logic;
        reset: in std_logic;
        pulsador: in std_logic;
        estado: out std_logic_vector (1 downto 0);
        peatones_v: out std_logic;
        peatones_r: out std_logic
    );
    end component;
    
    component sem2
    port(
        clk: in std_logic;
        reset: in std_logic;
        estado_sem1: in std_logic_vector (1 downto 0);
        estado: out std_logic_vector (1 downto 0);
        peatones_v: out std_logic;
        peatones_r: out std_logic
    );
    end component;
    
    component sem_to_7seg
    port(
        state : in std_logic_vector(1 downto 0);
        led : out std_logic_vector(6 downto 0)
    );
    end component;
    
    signal clk: std_logic := '0';
    signal new_clk: std_logic;
    signal reset, peatones_v_p1, peatones_r_p1, peatones_v_p2, peatones_r_p2: std_logic;
    signal peatones_v_s1, peatones_r_s1, peatones_v_s2, peatones_r_s2: std_logic;
    signal pulsador : std_logic := '0';
    signal estados_p1, estados_p2: std_logic_vector (1 downto 0);
    signal estados_s1, estados_s2: std_logic_vector (1 downto 0);
    signal led_p1, led_p2, led_s1, led_s2: std_logic_vector (6 downto 0);
begin
    inst_div_frecuencia: div_frec port map( --Divisor de frecuencia
        clk => clk,
        new_clk => new_clk
    );
    inst_semaforo_p1: sem1 port map( --Semaforo principal 1
        clk => clk,
        clk_cont => new_clk,
        reset => reset,
        pulsador => pulsador,
        estado => estados_p1,
        peatones_v => peatones_v_p1,
        peatones_r => peatones_r_p1
    );
    inst_decoder_p1: sem_to_7seg port map(
        state => estados_p1,
        led => led_p1
    );
    
    inst_semaforo_p2: sem1 port map( --Semaforo principal 2
        clk => clk,
        clk_cont => new_clk,
        reset => reset,
        pulsador => pulsador,
        estado => estados_p2,
        peatones_v => peatones_v_p2,
        peatones_r => peatones_r_p2
    );
    inst_decoder_p2: sem_to_7seg port map(
        state => estados_p2,
        led => led_p2
    );
    
    inst_semaforo_s1: sem2 port map(
        clk => clk,
        reset => reset,
        estado_sem1 => estados_p1,
        estado => estados_s1,
        peatones_v => peatones_v_s1,
        peatones_r => peatones_r_s1  
    );
    inst_decoder_s1: sem_to_7seg port map(
        state => estados_s1,
        led => led_s1
    );
    
    inst_semaforo_s2: sem2 port map(
        clk => clk,
        reset => reset,
        estado_sem1 => estados_p2,
        estado => estados_s2,
        peatones_v => peatones_v_s2,
        peatones_r => peatones_r_s2  
    );
    inst_decoder_s2: sem_to_7seg port map(
        state => estados_s2,
        led => led_s2
    );
    
    clk <= not clk after 5 ms;
    reset <= '1' after 0 ns, '0' after 100 ms;
    pulsador <= '1' after 200 ms, '0' after 300 ms, '1' after 1000 ms, '0' after 1100 ms, '1' after 1500ms, '0' after 1600ms;  
end Behavioral;