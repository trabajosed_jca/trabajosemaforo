----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.12.2018 17:14:57
-- Design Name: 
-- Module Name: sem_to_7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;

entity sem_to_7seg is
port (
    state : in std_logic_vector(1 DOWNTO 0);
    led : out std_logic_vector(6 DOWNTO 0)
);
end entity sem_to_7seg;
architecture dataflow of sem_to_7seg is
begin
    with state select
    led <=  "1110111" when "00",
            "1111110" when "01",
            "0111111" when "10",
            "0000000" when "11",
            "0000001" when others;
end architecture dataflow;
