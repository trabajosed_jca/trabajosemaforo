----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2019 17:27:32
-- Design Name: 
-- Module Name: prueba_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top_4sem_tb is
--  Port ( );
end top_4sem_tb;

architecture Behavioral of top_4sem_tb is
    component top_4sem
    port(
        clk: in std_logic;
        reset: in std_logic;
--        divider_clk: out std_logic;
--        display_clk: out std_logic;
        pulsador: in std_logic;
        peatones_v_p1: out std_logic;
        peatones_r_p1: out std_logic;
        peatones_v_p2: out std_logic;
        peatones_r_p2: out std_logic;
        peatones_v_s1: out std_logic;
        peatones_r_s1: out std_logic;
        peatones_v_s2: out std_logic;
        peatones_r_s2: out std_logic;
--        semaforo1: out std_logic_vector(6 downto 0);
--        semaforo2: out std_logic_vector(6 downto 0);
--        semaforo3: out std_logic_vector(6 downto 0);
--        semaforo4: out std_logic_vector(6 downto 0);
        semaforo: out std_logic_vector(6 downto 0);
        display: out std_logic_vector(7 downto 0)
    );
    end component;
    
    signal clk: std_logic := '0';
    signal reset, peatones_v_p1, peatones_r_p1, peatones_v_p2, peatones_r_p2: std_logic;
    signal peatones_v_s1, peatones_r_s1, peatones_v_s2, peatones_r_s2: std_logic;
    signal pulsador : std_logic := '0';
--    signal semaforo1, semaforo2, semaforo3, semaforo4: std_logic_vector (6 downto 0);
    signal semaforo: std_logic_vector (6 downto 0);
    signal display: std_logic_vector (7 downto 0);
begin
    inst_top: top_4sem port map(
        clk => clk,
        reset => reset,
--        divider_clk => div_clk,
--        display_clk => disp_clk,
        pulsador => pulsador,
        peatones_v_p1 => peatones_v_p1,
        peatones_r_p1 => peatones_r_p1,
        peatones_v_p2 => peatones_v_p2,
        peatones_r_p2 => peatones_r_p2,
        peatones_v_s1 => peatones_v_s1,
        peatones_r_s1 => peatones_r_s1,
        peatones_v_s2 => peatones_v_s2,
        peatones_r_s2 => peatones_r_s2,
--        semaforo1 => semaforo1,
--        semaforo2 => semaforo2,
--        semaforo3 => semaforo3,
--        semaforo4 => semaforo4,
        semaforo => semaforo,
        display => display
    );
    
    clk <= not clk after 5 ms;
    reset <= '1' after 0 ns, '0' after 100 ms;
   -- pulsador <= '1' after 200 ms, '0' after 300 ms, '1' after 1000 ms, '0' after 1100 ms, '1' after 1500ms, '0' after 1600ms;  
end Behavioral;
