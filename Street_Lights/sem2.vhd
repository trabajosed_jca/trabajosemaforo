----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2018 17:29:07
-- Design Name: 
-- Module Name: sem2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity sem2 is
    Port ( 
        clk: in std_logic;
        clk_cont: in std_logic; --Reloj para el contador
        reset: in std_logic;
        estado_sem1: in std_logic_vector (1 downto 0); --Entrada para sincronizar el estado de ambos semaforos
        estado: out std_logic_vector (1 downto 0);
        peatones_v: out std_logic;
        peatones_r: out std_logic
    );
end sem2;

architecture Behavioral of sem2 is
    type state_type is (S0, S1, S2, S3);
    signal state, next_state: state_type;
    signal cont: integer;
    signal ready: std_logic;
begin
    sync_proc: process (reset, clk)
    begin
        if reset = '0' then
            state <= S3;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;
    output_decode: process (state)
    begin
        case (state) is
            when S0 =>  --Rojo
                estado <= "10";
                peatones_v <= '1';
                peatones_r <= '0';                       
            when S1 =>  --�mbar
                estado <= "01";
                peatones_v <= '0';
                peatones_r <= '1'; 
            when S2 =>  --Verde
                estado <= "00";
                peatones_v <= '0';
                peatones_r <= '1';
            when S3 => 
                estado <= "11";
                peatones_v <= '0';
                peatones_r <= '0';
        end case;
    end process;
    next_state_decode: process (reset, estado_sem1, state)
    begin
    next_state <= state;
        case (state) is
            when S0 =>
                if estado_sem1 = "10" then --Cuando sem1 pasa a ambar, sem2 sigue rojo
                    next_state <= S2;
                end if;
            when S1 =>
                if estado_sem1 = "00" then --Cuando sem1 pasa a rojo, sem2 pasa a verde
                    next_state <= S0;
                end if;
            when S2 =>   --Cuando sem2 est� en verde y cumple tiempo, pasa a �mbar
                if ready = '1' then
                    next_state <= S1;
                end if;
            when S3 =>
                if(reset = '1') then
                    next_state <= S0;
                end if;
            when others => next_state <= S3;
        end case;
    end process;
    
    temporizador: process (reset, clk_cont, state)
    begin
        if reset = '0' then
            cont <= 0;
            ready <= '0';
        elsif rising_edge (clk_cont) then
            if state = S2 then
                cont <= cont + 1;
                if cont = 7 then
                    ready <= '1';
                    cont <= 0;
                end if;
            elsif state = S1 then
                ready <= '0';
            end if;
        end if; 
    end process;
end Behavioral;
