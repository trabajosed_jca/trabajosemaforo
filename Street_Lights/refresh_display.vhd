----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2019 16:49:20
-- Design Name: 
-- Module Name: refresh_display - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity refresh_display is
Port ( 
    clk: in std_logic;
    sem_p1: in std_logic_vector(6 downto 0); 
    sem_p2: in std_logic_vector(6 downto 0);
    sem_s1: in std_logic_vector(6 downto 0);
    sem_s2: in std_logic_vector(6 downto 0);
    semaforo_code: out std_logic_vector(6 downto 0);
    display_select: out std_logic_vector(7 downto 0)
);
end refresh_display;

architecture Behavioral of refresh_display is

begin
    muestra_displays:process (clk, sem_p1, sem_p2, sem_s1, sem_s2)
	variable cnt:integer range 0 to 3;
	begin
		if (clk'event and clk='1') then 
			if cnt=3 then
				cnt:=0;
			else
				cnt:=cnt+1;
			end if;
		end if;
		
		case cnt is
				when 0 => display_select <= "11111110"; 
                    semaforo_code <= sem_p1; 
                when 1 => display_select <= "11111101";
                    semaforo_code <= sem_p2; 
                when 2 => display_select <= "11111011";
                    semaforo_code <= sem_s1; 
                when 3 => display_select <= "11110111";
                    semaforo_code <= sem_s2;                                 
                end case;

	end process;

end Behavioral;
