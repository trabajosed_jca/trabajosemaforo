----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.01.2019 22:13:59
-- Design Name: 
-- Module Name: div_frec - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity div_frec is
    generic (
        frec : integer := 1000000000
    );
    Port (
        clk: in std_logic;
        new_clk: out std_logic
    );
end div_frec;

architecture Behavioral of div_frec is
    signal n_clk: std_logic := '0'; 
begin
    process (clk, n_clk)
    variable cont: integer := 0;
    begin
        if rising_edge(clk) then
            cont := cont+1;
            if cont = (frec/2) then
                n_clk <= not n_clk;
                cont := 0;
            end if;
        end if;
        new_clk <= n_clk;
    end process;
end Behavioral;
