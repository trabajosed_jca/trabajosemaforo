----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2018 19:30:29
-- Design Name: 
-- Module Name: top_4sem - Structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;

entity top_4sem is
port ( 
    clk: in std_logic;
--    divider_clk: out std_logic;
--    display_clk: out std_logic;
    reset: in std_logic;
    pulsador: in std_logic;
    peatones_v_p1: out std_logic;
    peatones_r_p1: out std_logic;
    peatones_v_p2: out std_logic;
    peatones_r_p2: out std_logic;
    peatones_v_s1: out std_logic;
    peatones_r_s1: out std_logic;
    peatones_v_s2: out std_logic;
    peatones_r_s2: out std_logic;
--    semaforo1: out std_logic_vector(6 downto 0);
--    semaforo2: out std_logic_vector(6 downto 0);
--    semaforo3: out std_logic_vector(6 downto 0);
--    semaforo4: out std_logic_vector(6 downto 0);
    semaforo: out std_logic_vector(6 downto 0);
    display: out std_logic_vector(7 downto 0)
);
end top_4sem;

architecture Structural of top_4sem is
signal estado_p1, estado_p2, estado_s1, estado_s2: std_logic_vector (1 downto 0);
signal new_clk, clk_display, pulsador_sync: std_logic;
signal led_p1, led_p2, led_s1, led_s2: std_logic_vector(6 DOWNTO 0);

component sincronizador
port(
    sync_in: in std_logic;
    clk: in std_logic;
    sync_out: out std_logic
);
end component;

component div_frec 
generic ( 
    frec : integer := 100000000 );
port(
    clk: in std_logic;
    new_clk: out std_logic
);
end component;

component sem1 
port(
    clk: in std_logic;
    clk_cont: in std_logic;
    reset: in std_logic;
    pulsador: in std_logic;
    estado: out std_logic_vector (1 downto 0);
    peatones_v: out std_logic;
    peatones_r: out std_logic
);
end component;

component sem2
port(
    clk: in std_logic;
    clk_cont: in std_logic; --Reloj para el contador
    reset: in std_logic;
    estado_sem1: in std_logic_vector (1 downto 0);
    estado: out std_logic_vector (1 downto 0);
    peatones_v: out std_logic;
    peatones_r: out std_logic
);
end component;

component sem_to_7seg
port(
    state : in std_logic_vector(1 downto 0);
    led : out std_logic_vector(6 downto 0)
);
end component;

component refresh_display is
port(
    clk: in std_logic;
    sem_p1: in std_logic_vector(6 downto 0); 
    sem_p2: in std_logic_vector(6 downto 0);
    sem_s1: in std_logic_vector(6 downto 0);
    sem_s2: in std_logic_vector(6 downto 0);
    semaforo_code: out std_logic_vector(6 downto 0);
    display_select: out std_logic_vector(7 downto 0)
);
end component;
begin
    inst_div_frecuencia: div_frec generic map( frec => 100000000) 
    port map( --Divisor de frecuencia
        clk => clk,
        new_clk => new_clk
    );
    inst_reloj_display: div_frec generic map( frec => 10000)
    port map(
        clk => clk,
        new_clk => clk_display   
    );
    inst_sincronizador: sincronizador port map(
        sync_in => pulsador,
        clk => clk,
        sync_out => pulsador_sync
    );
    inst_semaforo_p1: sem1 port map( --Semaforo principal 1
        clk => clk,
        clk_cont => new_clk,
        reset => reset,
        pulsador => pulsador_sync,
        estado => estado_p1,
        peatones_v => peatones_v_p1,
        peatones_r => peatones_r_p1 
    );
    inst_decoder_p1: sem_to_7seg port map(
        state => estado_p1,
        led => led_p1
    );
    
    inst_semaforo_p2: sem1 port map( --Semaforo principal 2
        clk => clk,
        clk_cont => new_clk,
        reset => reset,
        pulsador => pulsador_sync,
        estado => estado_p2,
        peatones_v => peatones_v_p2,
        peatones_r => peatones_r_p2 
    );
    inst_decoder_p2: sem_to_7seg port map(
        state => estado_p2,
        led => led_p2
    );
    
    inst_semaforo_s1: sem2 port map( --Semaforo secundario 1
       clk => clk,
       clk_cont => new_clk,
       reset => reset,
       estado_sem1 => estado_p1,
       estado => estado_s1,
       peatones_v => peatones_v_s1,
       peatones_r => peatones_r_s1  
    );
    inst_decoder_s1: sem_to_7seg port map(
        state => estado_s1,
        led => led_s1
    );
    
    inst_semaforo_s2: sem2 port map( --Semaforo secundario 2
       clk => clk,
       clk_cont => new_clk,
       reset => reset,
       estado_sem1 => estado_p2,
       estado => estado_s2,
       peatones_v => peatones_v_s2,
       peatones_r => peatones_r_s2  
    );
    inst_decoder_s2: sem_to_7seg port map(
        state => estado_s2,
        led => led_s2
    );
    inst_refresh_display: refresh_display port map(
        clk => clk_display,
        sem_p1 => led_p1, 
        sem_p2 => led_p2,
        sem_s1 => led_s1,
        sem_s2 => led_s2,
        semaforo_code => semaforo,
        display_select => display
    );
    
end Structural;
