----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.12.2018 18:15:10
-- Design Name: 
-- Module Name: sem1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity sem1 is
    Port ( 
        clk: in std_logic;
        clk_cont: in std_logic; --Reloj para el contador
        reset: in std_logic;
        pulsador: in std_logic;
        estado: out std_logic_vector (1 downto 0);
        peatones_v: out std_logic;
        peatones_r: out std_logic
    );
end sem1;

architecture Behavioral of sem1 is
    type state_type is (S0, S1, S2, S3);
    signal p_activo, p_enable: std_logic := '0';
    signal state, next_state: state_type;
    signal temp_v, temp_a, temp_r: std_logic;
    signal cont: integer := 0;
begin
    sync_proc: process (reset, clk)
    begin
        if reset = '0' then
            state <= S3;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;
    
    output_decode: process (state)
    begin
        case (state) is
            when S0 =>  --Verde
                estado <= "00";
                peatones_v <= '0';
                peatones_r <= '1';
                p_enable <= '1';                        
            when S1 =>  --Ambar
                estado <= "01";
                peatones_v <= '0';
                peatones_r <= '1';
                p_enable <= '0';       
            when S2 => --Rojo
                estado <= "10";
                peatones_v <= '1';
                peatones_r <= '0';
                p_enable <= '0';
            when S3 => 
                estado <= "11";
                peatones_v <= '0';
                peatones_r <= '0';
                p_enable <= '0';
        end case;
    end process;
    next_state_decode: process (state, temp_v, temp_a, temp_r)
    begin
    next_state <= state;
        case (state) is
            when S0 =>
                if pulsador = '1' then
                    p_activo <= '1';
                end if;
                if temp_v = '1' then
                    p_activo <= '0';
                    next_state <= S1;
                end if;
            when S1 =>
                if temp_a = '1' then
                    next_state <= S2;
                end if;
            when S2 =>
                if temp_r = '1' then
                    next_state <= S0;
                end if;
            when S3 =>
                if reset = '1' then
                    next_state <= S0;
                end if;
            when others => next_state <= S3;
        end case;
    end process;
--    activacion_pulsador: process (pulsador, temp_v, p_enable)
--    begin
--        if pulsador = '1' and p_enable = '1' then
--            p_activo <= '1';
--        elsif temp_v = '1' then
--            p_activo <= '0';
--        end if; 
--    end process;
    temporizador: process (reset, clk_cont , pulsador, state)
    begin
        if reset = '0' then
            cont <= 0;
            temp_v <= '0';
            temp_a <= '0';
            temp_r <= '0';
        elsif rising_edge (clk_cont) then
            cont <= cont + 1;
--            if p_activo = '1' and state = S0 and cont >= 3 then
--               temp_r <= '0';
--               temp_a <= '0';
--               temp_v <= '1';
--               cont <= 0; 
            if cont = 14 or (p_activo = '1' and cont >= 5) then
                temp_r <= '0';
                temp_a <= '0';
                temp_v <= '1';
                cont <= 0;
            elsif cont = 4 and temp_v = '1' then
                temp_v <= '0';
                temp_r <= '0';
                temp_a <= '1';
                cont <= 0;
            elsif cont = 9 and temp_a = '1' then
                temp_a <= '0';
                temp_v <= '0';
                temp_r <= '1';
                cont <= 0;
            end if;
        end if; 
    end process;
end Behavioral;
